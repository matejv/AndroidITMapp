package dn5.uv.fri.si.uv_naloga5;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

public class MainActivity extends ActionBarActivity {
    public static final String EXTRA_IME = "dn5.uv.fri.si.uv_naloga5.ime";
    public static final String EXTRA_PRIIMEK = "dn5.uv.fri.si.uv_naloga5.priimek";
    public static final String EXTRA_SPOL = "dn5.uv.fri.si.uv_naloga5.spol";
    public static final String EXTRA_VISINA = "dn5.uv.fri.si.uv_naloga5.visina";
    private final int REQUEST_ITM = 1;
    private final int REQUEST_DOZA = 2;

    EditText etIme1, etPriimek1, etVisina1;
    RadioGroup rgSpol1;
    RadioButton rbMoski1, rbZenski1;
    TextView tvITT1, tvITM1, tvDoza1;
    Button btITT1, btITM1, btDoza1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etIme1 = (EditText) findViewById(R.id.etIme1);
        etPriimek1 = (EditText) findViewById(R.id.etPriimek1);
        etVisina1 = (EditText) findViewById(R.id.etVisina1);
        rgSpol1 = (RadioGroup) findViewById(R.id.rgSpol1);
        rbMoski1 = (RadioButton) findViewById(R.id.rbMoski1);
        rbZenski1 = (RadioButton) findViewById(R.id.rbZenski1);
        tvITT1 = (TextView) findViewById(R.id.tvITT1);
        tvITM1 = (TextView) findViewById(R.id.tvITM1);
        tvDoza1 = (TextView) findViewById(R.id.tvDoza1);
        btITT1 = (Button) findViewById(R.id.btITT1);
        btITM1 = (Button) findViewById(R.id.btITM1);
        btDoza1 = (Button) findViewById(R.id.btDoza1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_itt1) {
            btITT1.performClick();
            return true;
        } else if (id == R.id.action_itm1) {
            btITM1.performClick();
            return true;
        } else if (id == R.id.action_doza1) {
            btDoza1.performClick();
            return true;
        } else if (id == R.id.action_ponastavi1) {
            ponastaviVnose();
            return true;
        } else if (id == R.id.action_izhod1) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void ponastaviBarve() {
        rbMoski1.setBackgroundColor(Color.TRANSPARENT);
        rbZenski1.setBackgroundColor(Color.TRANSPARENT);
        etVisina1.getBackground().clearColorFilter();
    }

    private void ponastaviVnose() {
        ponastaviBarve();

        etIme1.setText("");
        etPriimek1.setText("");
        rgSpol1.clearCheck();
        etVisina1.setText("");
        tvITT1.setText(R.string.main_0);
        tvITM1.setText(R.string.main_0);
        tvDoza1.setText(R.string.main_0);
    }

    private boolean preveriVnose() {
        ponastaviBarve();
        boolean vnosiOk = true;

        if (rgSpol1.getCheckedRadioButtonId() == -1) {
            rbMoski1.setBackgroundColor(Color.RED);
            rbZenski1.setBackgroundColor(Color.RED);
            vnosiOk = false;
        }

        if (etVisina1.getText().length() == 0) {
            etVisina1.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            vnosiOk = false;
        }

        return vnosiOk;
    }

    private void prikaziDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.nepopoln_vnos_naslov);
        dialog.setMessage(R.string.nepopoln_vnos_sporocilo);
        dialog.setNeutralButton(R.string.ok, null);
        dialog.show();
    }

    public void izracunajITT(View view) {
        if (preveriVnose()) {
            double visina = Double.parseDouble(etVisina1.getText().toString());
            double itt = 0.0;

            if (visina > 152) {
                itt = (visina - 152) / 2.54;
                itt *= 2.3;
            }

            if (rbMoski1.isChecked())
                itt += 50;
            else
                itt += 45.5;

            tvITT1.setText(String.format("%.1f", itt));
        } else {
            tvITT1.setText(R.string.main_0);
            prikaziDialog();
        }
    }

    public void izracunajITM(View view) {
        if (preveriVnose()) {
            Intent intent = new Intent(this, ITMActivity.class);
            intent.putExtra(EXTRA_IME, etIme1.getText().toString());
            intent.putExtra(EXTRA_PRIIMEK, etPriimek1.getText().toString());
            intent.putExtra(EXTRA_SPOL, rbMoski1.isChecked());
            intent.putExtra(EXTRA_VISINA, Double.parseDouble(etVisina1.getText().toString()));
            startActivityForResult(intent, REQUEST_ITM);
        } else {
            tvITM1.setText(R.string.main_0);
            prikaziDialog();
        }
    }

    public void izracunajDozo(View view) {
        String spol = "";

        if (rbMoski1.isChecked() && !rbZenski1.isChecked())
            spol = getResources().getString(R.string.moski);
        else if (!rbMoski1.isChecked() && rbZenski1.isChecked())
            spol = getResources().getString(R.string.zenska);

        Intent intent = new Intent(this, DozaActivity.class);
        intent.putExtra(EXTRA_IME, etIme1.getText().toString());
        intent.putExtra(EXTRA_PRIIMEK, etPriimek1.getText().toString());
        intent.putExtra(EXTRA_SPOL, spol);
        startActivityForResult(intent, REQUEST_DOZA);
    }

    @Override
    protected void onActivityResult(int reqC, int resC, Intent intent) {
        if (reqC == REQUEST_ITM) {
            if (resC == RESULT_OK) {
                double itm = intent.getDoubleExtra(ITMActivity.EXTRA_ITM, 0.0);
                tvITM1.setText(String.format("%.1f", itm));
            }
        } else if (reqC == REQUEST_DOZA) {
            if (resC == RESULT_OK) {
                double doza = intent.getDoubleExtra(DozaActivity.EXTRA_DOZA, 0.0);
                tvDoza1.setText(String.format("%.1f", doza));
            }
        }
    }
}
