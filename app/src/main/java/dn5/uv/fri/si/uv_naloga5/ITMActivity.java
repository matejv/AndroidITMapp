package dn5.uv.fri.si.uv_naloga5;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ITMActivity extends ActionBarActivity {
    public static final String EXTRA_ITM = "dn5.uv.fri.si.uv_naloga5.itm";

    TextView tvImePriimek2, tvSpol2;
    EditText etTeza2;
    Button btITM2, btPreklici2;

    double visina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itm);

        tvImePriimek2 = (TextView) findViewById(R.id.tvImePriimek2);
        tvSpol2 = (TextView) findViewById(R.id.tvSpol2);
        etTeza2 = (EditText) findViewById(R.id.etTeza2);
        btITM2 = (Button) findViewById(R.id.btITM2);
        btPreklici2 = (Button) findViewById(R.id.btPreklici2);

        Intent intent = getIntent();

        StringBuilder imePriimek = new StringBuilder();
        imePriimek.append(intent.getStringExtra(MainActivity.EXTRA_IME));
        imePriimek.append(" ");
        imePriimek.append(intent.getStringExtra(MainActivity.EXTRA_PRIIMEK));
        tvImePriimek2.setText(imePriimek.toString().trim());

        tvSpol2.setText(intent.getBooleanExtra(MainActivity.EXTRA_SPOL, true) ?
                R.string.moski : R.string.zenska);

        visina = intent.getDoubleExtra(MainActivity.EXTRA_VISINA, 0.0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_itm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_itm2) {
            btITM2.performClick();
            return true;
        } else if (id == R.id.action_preklici2) {
            btPreklici2.performClick();
            return true;
        } else if (id == R.id.action_ponastavi2) {
            ponastaviVnos();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void ponastaviBarve() {
        etTeza2.getBackground().clearColorFilter();
    }

    private void ponastaviVnos() {
        ponastaviBarve();

        etTeza2.setText("");
    }

    private boolean preveriVnos() {
        ponastaviBarve();
        boolean vnosOk = true;

        if (etTeza2.getText().length() == 0) {
            etTeza2.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            vnosOk = false;
        }

        return vnosOk;
    }

    public void potrdiITM(View view) {
        if (preveriVnos()) {
            double teza = Double.parseDouble(etTeza2.getText().toString());
            visina /= 100; // iz cm v m

            double itm = teza / (visina * visina);

            Intent intent = new Intent();
            intent.putExtra(EXTRA_ITM, itm);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle(R.string.nepopoln_vnos_naslov);
            dialog.setMessage(R.string.nepopoln_vnos_sporocilo);
            dialog.setNeutralButton(R.string.ok, null);
            dialog.show();
        }
    }

    public void prekliciITM(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }
}
