package dn5.uv.fri.si.uv_naloga5;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class DozaActivity extends ActionBarActivity {
    public static final String EXTRA_DOZA = "dn5.uv.fri.si.uv_naloga5.doza";

    TextView tvImePriimek3, tvSpol3;
    Spinner spDozaOd3, spDozaDo3;
    EditText etKolicina3, etEnot3, etTeza3;
    Button btDoza3, btPreklici3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doza);

        tvImePriimek3 = (TextView) findViewById(R.id.tvImePriimek3);
        tvSpol3 = (TextView) findViewById(R.id.tvSpol3);
        spDozaOd3 = (Spinner) findViewById(R.id.spDozaOd3);
        spDozaDo3 = (Spinner) findViewById(R.id.spDozaDo3);
        etKolicina3 = (EditText) findViewById(R.id.etKolicina3);
        etEnot3 = (EditText) findViewById(R.id.etEnot3);
        etTeza3 = (EditText) findViewById(R.id.etTeza3);
        btDoza3 = (Button) findViewById(R.id.btDoza3);
        btPreklici3 = (Button) findViewById(R.id.btPreklici3);

        Intent intent = getIntent();

        StringBuilder imePriimek = new StringBuilder();
        imePriimek.append(intent.getStringExtra(MainActivity.EXTRA_IME));
        imePriimek.append(" ");
        imePriimek.append(intent.getStringExtra(MainActivity.EXTRA_PRIIMEK));
        tvImePriimek3.setText(imePriimek.toString().trim());

        tvSpol3.setText(intent.getStringExtra(MainActivity.EXTRA_SPOL));

        List<Integer> dozaOd = new ArrayList<Integer>();

        for (int i = 5; i <= 100; i += 5)
            dozaOd.add(i);

        ArrayAdapter<Integer> adpOd = new ArrayAdapter<Integer>(this,
                android.R.layout.simple_spinner_item, dozaOd);
        adpOd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDozaOd3.setAdapter(adpOd);

        spDozaOd3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                List<Integer> dozaDo = new ArrayList<Integer>();
                int izbranoOd = Integer.parseInt(spDozaOd3.getSelectedItem().toString());

                for (int i = izbranoOd; i <= izbranoOd + 100; i += 5)
                    dozaDo.add(i);

                ArrayAdapter<Integer> adpDo = new ArrayAdapter<Integer>(view.getContext(),
                        android.R.layout.simple_spinner_item, dozaDo);
                adpDo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spDozaDo3.setAdapter(adpDo);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_doza, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_doza3) {
            btDoza3.performClick();
            return true;
        } else if (id == R.id.action_preklici3) {
            btPreklici3.performClick();
            return true;
        } else if (id == R.id.action_ponastavi3) {
            ponastaviVnose();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void ponastaviBarve() {
        etKolicina3.getBackground().clearColorFilter();
        etEnot3.getBackground().clearColorFilter();
        etTeza3.getBackground().clearColorFilter();
    }

    private void ponastaviVnose() {
        ponastaviBarve();

        spDozaOd3.setSelection(0);
        spDozaDo3.setSelection(0);
        etKolicina3.setText("");
        etEnot3.setText("");
        etTeza3.setText("");
    }

    private boolean preveriVnose() {
        ponastaviBarve();
        boolean vnosiOk = true;

        if (etKolicina3.getText().length() == 0) {
            etKolicina3.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            vnosiOk = false;
        }

        if (etEnot3.getText().length() == 0) {
            etEnot3.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            vnosiOk = false;
        }

        if (etTeza3.getText().length() == 0) {
            etTeza3.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            vnosiOk = false;
        }

        return vnosiOk;
    }

    public void potrdiDozo(View view) {
        if (preveriVnose()) {
            double doza = Double.parseDouble(spDozaOd3.getSelectedItem().toString());
            doza *= Double.parseDouble(spDozaDo3.getSelectedItem().toString());
            doza /= Double.parseDouble(etKolicina3.getText().toString());
            doza *= Double.parseDouble(etEnot3.getText().toString());
            doza /= Double.parseDouble(etTeza3.getText().toString());

            Intent intent = new Intent();
            intent.putExtra(EXTRA_DOZA, doza);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle(R.string.nepopoln_vnos_naslov);
            dialog.setMessage(R.string.nepopoln_vnos_sporocilo);
            dialog.setNeutralButton(R.string.ok, null);
            dialog.show();
        }
    }

    public void prekliciDozo(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }
}
